﻿namespace Walker
{
    public enum AllOrOneOfRequired
    {
        AllOf,
        OneOf
    }
}