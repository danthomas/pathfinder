﻿using System.Collections.Generic;
using System.Text;

namespace Walker
{
    public class Node
    {
        public Node()
        {
            Nodes = new List<Node>();
        }

        public List<Node> Nodes { get; set; }
        public Element Element { get; set; }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            ToString(this, stringBuilder, 0);

            return stringBuilder.ToString().Trim();
        }

        private void ToString(Node node, StringBuilder stringBuilder, int indent)
        {
            stringBuilder.Append(new string(' ', indent * 4));
            stringBuilder.AppendLine(node.Element.Name);
            foreach (var child in node.Nodes)
            {
                ToString(child, stringBuilder, indent + 1);
            }
        }
    }
}