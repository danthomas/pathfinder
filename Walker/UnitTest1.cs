﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Walker
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test()
        {
            /*
             * Given the following tree which is a sort of Backus-Nauf form like notation.
             * | indicates or, so B is F or G
             * [] indicate optional, so H is optional
             *
             *  Def: A B C
             *  A: D E
             *  B: F | G
             *  C: [H] I
             *  D: a b
             *  E: c d
             *  F: e f
             *  G: g h
             *  H: i j
             *  I: k l
             *
             *  a: a
             *  b: b
             *  c: c
             *  d: d
             *  e: e
             *  f: f
             *  g: g
             *  h: h
             *  i: i
             *  j: j
             *  k: k
             *  l: l
             *
             * I need to walk the tree extracting the leaf nodes and convert to the following tree
             * which gives the possible routes
             *
             *  Def
             *      a
             *          b
             *              c
             *                  d
             *                      e
             *                          f
             *                              i
             *                                  j
             *                                      k
             *                                          l
             *                              k
             *                                  l
             *                      g
             *                          h
             *                              i
             *                                  j
             *                                      k
             *                                          l
             *                              k
             *                                  l
             *
             */


            var definition = new Element { ElementType = ElementType.Definition };

            #region setup
            var statementDef = new Element { ElementType = ElementType.Statement, Name = "Def" };
            definition.Elements.Add(statementDef);
            var statementA = new Element { ElementType = ElementType.Statement, Name = "A" };
            definition.Elements.Add(statementA);
            var statementB = new Element { ElementType = ElementType.Statement, Name = "B" };
            definition.Elements.Add(statementB);
            var statementC = new Element { ElementType = ElementType.Statement, Name = "C" };
            definition.Elements.Add(statementC);
            var statementD = new Element { ElementType = ElementType.Statement, Name = "D" };
            definition.Elements.Add(statementD);
            var statementE = new Element { ElementType = ElementType.Statement, Name = "E" };
            definition.Elements.Add(statementE);
            var statementF = new Element { ElementType = ElementType.Statement, Name = "F" };
            definition.Elements.Add(statementF);
            var statementG = new Element { ElementType = ElementType.Statement, Name = "G" };
            definition.Elements.Add(statementG);
            var statementH = new Element { ElementType = ElementType.Statement, Name = "H" };
            definition.Elements.Add(statementH);
            var statementI = new Element { ElementType = ElementType.Statement, Name = "I" };
            definition.Elements.Add(statementI);

            var tokenA = new Element { ElementType = ElementType.Token, Name = "a" };
            definition.Elements.Add(tokenA);
            var tokenB = new Element { ElementType = ElementType.Token, Name = "b" };
            definition.Elements.Add(tokenB);
            var tokenC = new Element { ElementType = ElementType.Token, Name = "c" };
            definition.Elements.Add(tokenC);
            var tokenD = new Element { ElementType = ElementType.Token, Name = "d" };
            definition.Elements.Add(tokenD);
            var tokenE = new Element { ElementType = ElementType.Token, Name = "e" };
            definition.Elements.Add(tokenE);
            var tokenF = new Element { ElementType = ElementType.Token, Name = "f" };
            definition.Elements.Add(tokenF);
            var tokenG = new Element { ElementType = ElementType.Token, Name = "g" };
            definition.Elements.Add(tokenG);
            var tokenH = new Element { ElementType = ElementType.Token, Name = "h" };
            definition.Elements.Add(tokenH);
            var tokenI = new Element { ElementType = ElementType.Token, Name = "i" };
            definition.Elements.Add(tokenI);
            var tokenJ = new Element { ElementType = ElementType.Token, Name = "j" };
            definition.Elements.Add(tokenJ);
            var tokenK = new Element { ElementType = ElementType.Token, Name = "k" };
            definition.Elements.Add(tokenK);
            var tokenL = new Element { ElementType = ElementType.Token, Name = "l" };
            definition.Elements.Add(tokenL);

            statementDef.Elements.Add(new Element { ElementType = ElementType.Group });
            statementDef.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementDef.Elements[0].Elements[0].Elements.Add(statementA);
            statementDef.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementDef.Elements[0].Elements[1].Elements.Add(statementB);
            statementDef.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementDef.Elements[0].Elements[2].Elements.Add(statementC);
            statementA.Elements.Add(new Element { ElementType = ElementType.Group });
            statementA.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementA.Elements[0].Elements[0].Elements.Add(statementD);
            statementA.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementA.Elements[0].Elements[1].Elements.Add(statementE);
            statementB.Elements.Add(new Element { ElementType = ElementType.Group, AllOrOneOfRequired = AllOrOneOfRequired.OneOf });
            statementB.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementB.Elements[0].Elements[0].Elements.Add(statementF);
            statementB.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementB.Elements[0].Elements[1].Elements.Add(statementG);
            statementC.Elements.Add(new Element { ElementType = ElementType.Group });
            statementC.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference, Optional = true});
            statementC.Elements[0].Elements[0].Elements.Add(statementH);
            statementC.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementC.Elements[0].Elements[1].Elements.Add(statementI);
            statementD.Elements.Add(new Element { ElementType = ElementType.Group });
            statementD.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementD.Elements[0].Elements[0].Elements.Add(tokenA);
            statementD.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementD.Elements[0].Elements[1].Elements.Add(tokenB);
            statementE.Elements.Add(new Element { ElementType = ElementType.Group });
            statementE.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementE.Elements[0].Elements[0].Elements.Add(tokenC);
            statementE.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementE.Elements[0].Elements[1].Elements.Add(tokenD);
            statementF.Elements.Add(new Element { ElementType = ElementType.Group });
            statementF.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementF.Elements[0].Elements[0].Elements.Add(tokenE);
            statementF.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementF.Elements[0].Elements[1].Elements.Add(tokenF);
            statementG.Elements.Add(new Element { ElementType = ElementType.Group });
            statementG.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementG.Elements[0].Elements[0].Elements.Add(tokenG);
            statementG.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementG.Elements[0].Elements[1].Elements.Add(tokenH);
            statementH.Elements.Add(new Element { ElementType = ElementType.Group });
            statementH.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementH.Elements[0].Elements[0].Elements.Add(tokenI);
            statementH.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementH.Elements[0].Elements[1].Elements.Add(tokenJ);
            statementI.Elements.Add(new Element { ElementType = ElementType.Group });
            statementI.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementI.Elements[0].Elements[0].Elements.Add(tokenK);
            statementI.Elements[0].Elements.Add(new Element { ElementType = ElementType.Reference });
            statementI.Elements[0].Elements[1].Elements.Add(tokenL);
            #endregion


            var asString = definition.ToString();

            var node = new Walker().Walk(definition.Elements[0]);
            var actual = node.ToString();
            Assert.AreEqual(@"Def
    a
        b
            c
                d
                    e
                        f
                            i
                                j
                                    k
                                        l
                            k
                                l
                    g
                        h
                            i
                                j
                                    k
                                        l
                            k
                                l", actual);
        }
    }
}
