﻿namespace Walker
{
    public enum NumberRequired
    {
        One,
        OneOrMore,
        ZeroOrMore
    }
}