﻿namespace Walker
{
    public class Walker
    {
        public Node Walk(Element element)
        {
            var node = new Node { Element = element };
            var root = node;
            Walk(element, ref node);
        
            return root;
        }
        
        private void Walk(Element parentElement, ref Node parent)
        {
            foreach (var childElement in parentElement.Elements)
            {
                switch (childElement.ElementType)
                {
                    case ElementType.Group:
                    case ElementType.Statement:
                    case ElementType.Reference:
                        Walk(childElement, ref parent);
                        break;
                    case ElementType.Regex:
                    case ElementType.Token:
                        var child = new Node { Element = childElement };
                        parent.Nodes.Add(child);
                        parent = child;
                        break;
                }
            }
        }
    }
}