﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Walker
{
    public class Element
    {
        public Element()
        {
            Elements = new List<Element>();
        }

        public Element(ElementType elementType, string name, string text = "", bool optional = false,
            AllOrOneOfRequired allOrOneOfRequired = AllOrOneOfRequired.AllOf,
            NumberRequired numberRequired = NumberRequired.One)
        {
            ElementType = elementType;
            Name = name;
            Text = text;
            Optional = optional;
            AllOrOneOfRequired = allOrOneOfRequired;
            NumberRequired = numberRequired;
            Elements = new List<Element>();
        }


        public Element AddElement(ElementType elementType, string name, string text = "", bool optional = false,
            AllOrOneOfRequired allOrOneOfRequired = AllOrOneOfRequired.AllOf,
            NumberRequired numberRequired = NumberRequired.One)
        {
            var element = new Element(elementType, name, text, optional, allOrOneOfRequired, numberRequired);
            Elements.Add(element);
            return element;
        }

        public string Name { get; set; }
        public string Text { get; set; }
        public ElementType ElementType { get; set; }
        public List<Element> Elements { get; set; }
        public bool Optional { get; set; }
        public AllOrOneOfRequired AllOrOneOfRequired { get; set; }
        public NumberRequired NumberRequired { get; set; }
        public bool Ignore { get; set; }

        public override string ToString()
        {
            switch (ElementType)
            {
                case ElementType.Definition:
                    return Name + Environment.NewLine + String.Join(Environment.NewLine, Elements.Where(x => x.ElementType == ElementType.Statement).Select(x => x.ToString()))
                           + Environment.NewLine + Environment.NewLine + String.Join(Environment.NewLine, Elements.Where(x => x.ElementType == ElementType.Regex).Select(x => x.ToString()))
                           + Environment.NewLine + Environment.NewLine + String.Join(Environment.NewLine, Elements.Where(x => x.ElementType == ElementType.Token).Select(x => x.ToString()));
                case ElementType.Statement:
                    return Name + (Ignore ? "~" : "") + ": " + String.Join(AllOrOneOfRequired == AllOrOneOfRequired.AllOf ? " " : " | ",
                               Elements.Select(x => x.ToString()));
                case ElementType.Regex:
                    return Name + (Ignore ? "~" : "") + ": /" + Text + "/";
                case ElementType.Token:
                    return Name + (Ignore ? "~" : "") + ": " + (String.IsNullOrWhiteSpace(Text) ? Name : Text);
                case ElementType.Reference:
                    return WrapInModifiers(() => Elements[0].Name);
                case ElementType.Group:
                    return WrapInModifiers(() => String.Join(AllOrOneOfRequired == AllOrOneOfRequired.AllOf ? " " : " | ", Elements.Select(x => x.ToString())));
            }

            throw new Exception();
        }

        private string WrapInModifiers(Func<string> func)
        {
            return (Optional ? "[" : "")
                   + func()
                   + (Optional ? "]" : "")
                   + (NumberRequired == NumberRequired.OneOrMore ? "+" : NumberRequired == NumberRequired.ZeroOrMore ? "*" : "");
        }
    }
}