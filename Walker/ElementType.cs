﻿namespace Walker
{
    public enum ElementType
    {
        Definition,
        Statement,
        Regex,
        Token,
        Reference,
        Group
    }
}